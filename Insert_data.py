from Connect_SQLite import *
from read import readFilm


from time import sleep
#create a subroutine to add songs to the songs table in the database GLA1.db
def addFilm():
    # create an empty list 
    film = []

    # capture user data 
    title = input("Enter film Title: ")
    yearReleased = input("Enter year released: ")
    rating = input("Enter film rating: ")
    duration = input("Enter film duration: ")
    genre = input("Enter film Genre: ")


    film.append(title)
    film.append(yearReleased)
    film.append(rating)
    film.append(duration)
    film.append(genre)

    # insert data appended to the list above into the songs table 
    cursor_2.execute("INSERT INTO tblfilms VALUES(NULL, ?, ?, ?, ?, ?)",  film)
    conn_2.commit()
    print(f"{title} added to the tblfilms table") 
    sleep(2) # delay three second before executing the next line of code
    
    readFilm()
    # cursor.execute("SELECT * FROM songs") # select all records from the songs table
    # row = cursor.fetchall() # fetch all the songs that was selected above and pass/store it to the row variable
    # for record in row: # iterate through the records ( held in the row variable)
    #     print(record)

# addFilm() 